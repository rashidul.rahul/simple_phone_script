import os
import time
import pymongo
import csv

MENU_ITEMS = ("A", "F", "E",)


def read_from_mongodb():
    pass
    # mongo_client = env.str("MONGO_CLIENT")


    # client = pymongo.MongoClient(mongo_client)
    # db = client.phone_database
    # collection = db.phones
    # return collection.find()

def get_file_name():
    file_name = input("Please input file name\n")
    if file_name == "":
        return get_file_name()
    file_name.replace(" ", "_")
    return file_name


def read_txt(file_name):
    file_data = open(file_name, "r").read().splitlines()
    return file_data
        

def read_csv(file_name):
    data = []
    with open(file_name) as csv_file:
        csv_reader = csv.reader(csv_file)
        next(csv_reader)
        for row in csv_reader:
            data.append(row[0])
    return data


def menus():
    value = "o"
    print("SELECT MENU:")
    print("Press F for filter numbers.")
    print("Press A for adding 88 before the numbers.")
    print("Press E for Exit.")
    value = input("\n")
    if value.upper() not in MENU_ITEMS:
        print("You did not enter anything or Invalid Charecter. please enter again...\n")
        menus()
    return value.upper()


def create_folder():
    create = False
    if not os.path.exists("input"):
        os.mkdir("input")
        create = True
    if not os.path.exists("phone_list"):
        os.mkdir("phone_list")
        create = True
    return create


def read_file(file_name):
    _, ext = os.path.splitext(file_name)
    if ext == ".txt":
        return read_txt(file_name)
    elif ext == ".csv":
        return read_csv(file_name)
    return None


def get_base_and_dnd(not_found=None):
    if not_found:
        input("{} file not found. please make sure there is a file named {}".format(not_found, not_found))
    folder_name = "input/"
    file_list = os.listdir(folder_name)
    dnd_file = None
    base_file = None
    for f in file_list:
        if "dnd" in f:
            dnd_file = folder_name + f
        if "base" in f:
            base_file = folder_name + f
    if not dnd_file:
        return get_base_and_dnd("dnd")
    if not base_file:
        return get_base_and_dnd("main base")
    
    dnd_base = read_file(dnd_file)
    main_base = read_file(base_file)
    
    if not dnd_base or  not main_base:
        input("Unknown file system file type must be csv or txt")
        time.sleep(5)
        return
    return main_base, dnd_base


def store_base(data, file_name, direct=False):
    store_file = file_name + ".txt"
    write_file = open(store_file, "w")
    new_line = False
    for d in data:
        if new_line:
            write_file.write("\n")
        write_file.write(d)
        new_line = True

    write_file.close()

    if not direct:
        sp = int(len(data)/2)
        splt_data = data[:sp]
        tmp = file_name + "_a"
        store_base(splt_data, tmp, direct=True)
        splt_data = data[sp:]
        tmp = file_name + "_b"
        store_base(splt_data, tmp, direct=True)
    return


def process_store_base(base, split_number):
    print("Total filtered base: {}".format(len(base)))
    folder_name = "phone_list/"

    file_name = folder_name + get_file_name()
    file_count = 1
    main_file = []
    # print(file_name)
    print("Starting store base.........")

    for i, num in enumerate(base):
        main_file.append(num)
        if i !=0 and len(main_file) % split_number == 0:
            tmp_file = file_name + "_" +  str(file_count)
            store_base(main_file, tmp_file)
            file_count += 1
            main_file = []
    
    if main_file:
        tmp_file = file_name + "_" +  str(file_count)
        store_base(main_file, tmp_file, direct=True)
    print("-----------------Job Done------------------------------")
    return


def dnd_search(item_list,item):
	first = 0
	last = len(item_list)-1
	found = False
	while( first<=last and not found):
		mid = (first + last)//2
		if item_list[mid] == item :
			found = True
		else:
			if item < item_list[mid]:
				last = mid - 1
			else:
				first = mid + 1	
	return found


def filter_number(value=40000):
    phones, dnd_base = get_base_and_dnd()
    print("\n Total DND base is {}".format(str(len(dnd_base))))
    print("\n Total base is {}".format(str(len(phones))))
    filter_base = []
    dnd_count = 0
    mnp_count = 0
    for i, num in enumerate(phones):
        if num.startswith("88"):
            num = num[3:]
        if num[:2] not in ("16", "18"):
            mnp_count += 1
            continue
        if dnd_search(dnd_base, num):
            dnd_count += 1
            continue
        filter_base.append(num)
        if i != 0 and (i) % 1000000 == 0:
            print("Filter {} number".format(str(i)))
    print("Total dnd found {}".format(dnd_count))
    print("Total mnp found {}".format(mnp_count))
    
    process_store_base(filter_base, value)

    return


def start_work(value):
    if value == "F":
        number = input("""How many number do you want to keep in a file(For default(40000) please leave blank)?\n""")
        if number:
            try:
                number = int(number)
            except ValueError:
                os.system("clear")
                print("\nYou Must Enter a Number or blank for default...\n")
                return start_work(value)
            if number == 0:
                os.system("clear")
                print("\nNumber must not be 0. Try again\n")
                return start_work(value)
            elif number % 2 != 0:
                os.system("clear")
                print("\nNumber must be divisible by 2. Try again\n")
                return start_work(value)
            return filter_number(int(number))

        else:
            return filter_number()

    if value == "A":
        return


def start():
    os.system("clear")
    if create_folder():
        print("All folders are created")
    menu = menus()
    if menu == "E":
        return

    start_work(menu)
    time.sleep(3)
    os.system("clear")
    start()


if __name__ == "__main__":    
    start()