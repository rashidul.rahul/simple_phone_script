How to use this

Windows
1. Install python3
2. Install pip
3. Run command on cmd: pip install virtualenv
4. clone this repo
5. Open cmd on that repo folder
6. Run: virtualenv env
7. Run: env\Scripts\activate 
8. Run: pip install requirements.txt
9. Make a file named .env like .env.sample 
10. Run: python script.py 
